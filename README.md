### Model than abstracts the creation of the resources, thus making a pattern to use ngResource easily and quickly.

### How to install
`bower i yoda-resource-model`

# Dependencies
* AngularJS
* ngResource
